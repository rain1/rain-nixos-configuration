{ config, pkgs, ... }:

{
  users = {
    mutableUsers = false;
    extraUsers = [
      {
        uid             = 1000;
        name            = "nix";
        group           = "users";
        extraGroups     = [ "wheel" "fuse" ];
        isNormalUser    = true;
        password        = "nix";
      }
    ];
  };
}
