{ config, pkgs, ... }:

{
  systemd.services."remove-mnt-directories" =
    { description = "Delete ~/mnt/* directories";
      wantedBy = [ "multi-user.target" ];
#      wantedBy = [ "multi-user.target" ]; default.target
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "/bin/sh -c 'rmdir /home/nix/mnt/* || exit'";
      };
    };
}
