{ config, pkgs, ... }:

{
  # nix-env -q | sed -e 's/-[^-]*$//' | sed -e 's/^/    /'
  environment.systemPackages = with pkgs; [
    chromium
    desktop-file-utils
    dosbox
    emacs
    ffmpeg
    ffmpegthumbnailer
    file
    git
    go
    lldb
    mpv
    mupdf
    nasm
    nix-index
    ntfs3g
    p7zip
    pari
    pavucontrol
    pcmanfm
    qemu
    quiterss
    racket
    scummvm
    sshfs-fuse
    wget
    xarchiver
    xfce.xfce4-screenshooter
    youtube-dl
  ];
  fonts.fonts = with pkgs; [
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    liberation_ttf
    fira-code
    fira-code-symbols
    mplus-outline-fonts
    dina-font
    proggyfonts
  ];
}
