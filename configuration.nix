{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ./conf/packages-and-fonts.nix
      ./conf/users.nix
      ./conf/systemd-units.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sdc";

  i18n.consoleFont = "Lat2-Terminus16";
  i18n.consoleKeyMap = "us";
  i18n.defaultLocale = "en_US.UTF-8";
  
  time.timeZone = "GMT";

  # https://www.opennic.org/
  networking.nameservers = ["208.67.222.222"];
  networking.interfaces.eth0 = {
    name = "eth0";
    ipv4.addresses = [ { address = "192.168.1.22"; prefixLength = 24; } ];
  };
  networking.interfaces.tap0 = {
    name = "tap0";
    virtual = true; # needed?
    virtualType = "tap";
    virtualOwner = "nix";
  };
  networking.interfaces.br0 = {
    name = "br0";
    ipv4.addresses = [ { address = "192.168.1.44"; prefixLength = 24; } ];
  };
  networking.defaultGateway = "192.168.1.254";
  networking.bridges.br0.interfaces = [ "enp2s0" "tap0" ];
  
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  
  services.openssh.enable = true;
  services.openssh.passwordAuthentication = false;
  services.openssh.ports = [1221];
  services.openssh.permitRootLogin = "no";

  services.xserver.enable = true;
  services.xserver.layout = "us";
  services.xserver.xkbOptions = "eurosign:e";
  services.xserver.xkbVariant = "dvorak";
  services.xserver.desktopManager.xfce.enable = true;
  services.xserver.videoDrivers = [ "intel" ];
  services.xserver.deviceSection = ''
  Option		"TearFree"	"true"
  Option		"AccelMethod"	"sna"
  '';
#    Option "DRI" "2"

  services.dbus.enable = true;
  
  environment.interactiveShellInit = ''
    export PATH="$PATH:$HOME/Programming/bin"
    export XDG_DATA_DIRS=$XDG_DATA_DIRS:$HOME/.nix-profile/share
  '';
  
  environment.etc."fuse.conf" = { text = ''
    user_allow_other
  '';};

  nix.autoOptimiseStore = true;
  
  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.09";
  system.autoUpgrade.enable = true;
}
